using UnityEngine;

namespace CaM
{
    public class ExitApplication : MonoBehaviour
    {
        public void EndProgram() {
            Debug.Log("Ending game...", gameObject);
            if (Application.isEditor) Debug.LogWarning("You're in the editor, so you'd probs just end Play Mode now.", this);
            Application.Quit();
        }
    }
}
