using UnityEngine;
using UnityEngine.UI;

namespace CaM.UI
{
    public class UIMasterVolumeControl : MonoBehaviour
    {
        // Sets the float to PlayerPrefs, which is stored in the Registry files on Windows
        private void SetFloat(string KeyName, float Value) {
            PlayerPrefs.SetFloat(KeyName, Value);
        }

        // Uses the key to get the float stored in PlayerPrefs
        private float GetFloat(string KeyName) {
            return PlayerPrefs.GetFloat(KeyName);
        }

        const string k_SettingsMasterVolume = "AUDIO_MasterVolume";
        Slider audioSlider;

        private void Awake() {
            // If settings does not exist, it will set it at full volume
            // if settings are found, it will set the audio settings
            if (!PlayerPrefs.HasKey(k_SettingsMasterVolume))
                SetFloat(k_SettingsMasterVolume, 1f);

            // Get Slider component
            audioSlider = GetComponent<Slider>();

            // Get the volume
            float vol = GetFloat(k_SettingsMasterVolume);
            // make sure the settings globally are set
            AudioListener.volume = vol;
            // Set the Slider value for what the audio was set to
            audioSlider.value = vol;
        }

        // Set the audio settings (without audio float input)
        public void OnVolumeChange() {
            float sliderValue = audioSlider.value;
            AudioListener.volume = sliderValue;
            SetFloat(k_SettingsMasterVolume, sliderValue);

            Debug.Log("UIMasterVolumeControl: Master Volume now set to - " + sliderValue);
        }
    }
}
