using Unity.Netcode;
using UnityEngine;

using CaM.Game.Networking;

namespace CaM.UI
{
    public class GameStartMenuManager : MonoBehaviour
    {
        // temp: will have map selection when the time comes
        public int MapToLoad = 6;

        // Takes all UI input needed to host a server
        public void HostServer() {
            Debug.Log("Success Host Server Press");
            if (NetworkManager.Singleton == null)
            {
                if (Application.isEditor)
                    Debug.LogWarning("GameStartMenuManager: NetworkManager is not active in the scene, please restart game in '_ManagersScene'.");
                return;
            }

            LoadingData.isServer = true;
            LoadingData.isClient = true;
            LoadingData.SceneIdOfMapToLoad = MapToLoad;

            GameNetworkSceneManager.Instance.LoadGameFromStartMenu(MapToLoad);
        }

        // Takes all UI input needed to start a server
        public void StartServer() {
            Debug.Log("Success Start Server Press");
            if (NetworkManager.Singleton == null)
            {
                if (Application.isEditor)
                    Debug.LogWarning("GameStartMenuManager: NetworkManager is not active in the scene, please restart game in '_ManagersScene'.");
                return;
            }

            LoadingData.isServer = true;
            LoadingData.SceneIdOfMapToLoad = MapToLoad;

            GameNetworkSceneManager.Instance.LoadGameFromStartMenu(MapToLoad);
        }

        // Takes all UI input needed to join a server
        public void JoinServer() {
            Debug.Log("Success Join Server Press");
            if (NetworkManager.Singleton == null)
            {
                if (Application.isEditor)
                    Debug.LogWarning("GameStartMenuManager: NetworkManager is not active in the scene, please restart game in '_ManagersScene'.");
                return;
            }

            LoadingData.isClient = true;
            LoadingData.SceneIdOfMapToLoad = MapToLoad;

            GameNetworkSceneManager.Instance.LoadGameFromStartMenu(MapToLoad);
        }
    }
}
