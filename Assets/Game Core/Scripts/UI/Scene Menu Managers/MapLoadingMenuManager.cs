using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

using CaM.Game.Networking;

namespace CaM.UI
{
    public class MapLoadingMenuManager : MonoBehaviour
    {
        private string m_loadingScreenSceneName;
        private GameNetworkSceneManager m_gameSceneManager;

        [SerializeField]
        private RectTransform m_loadingBarRect;
        private float m_fullWidthOfLoadingBar;
        [SerializeField]
        private TMP_Text m_loadingText;

        private void Awake() {
            m_gameSceneManager = GameNetworkSceneManager.Instance;
            m_loadingScreenSceneName = GameNetworkSceneManager.GameDefaultSceneNames._MapLoadingScene.ToString();
            m_fullWidthOfLoadingBar = m_loadingBarRect.rect.width;

            if (m_gameSceneManager == null)
                SceneManager.LoadScene(0);
        }

        private void OnEnable() {
            SceneManager.sceneLoaded += menuLoadFinished;
        }

        //Checks for when the menu has finished loading
        private void menuLoadFinished(Scene arg0, LoadSceneMode arg1) {
            Debug.Log("MapLoadingMenuManager: '" + arg0.name + "' has been loaded");
        }

        public void LoadingScreenFinished(string s) {
            if (s == "LoadingScreenAnimationEnded")
            {
                SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(m_loadingScreenSceneName));
            }
        }

        private void OnDisable() {
            SceneManager.sceneLoaded -= menuLoadFinished;
        }

        private void Update() {
            if (m_gameSceneManager.CurrentAsyncLoadPercentage >= 0f && m_gameSceneManager.CurrentAsyncLoadPercentage <= 1f)
            {
                m_loadingText.text = string.Format("{0:0.00%}", (m_gameSceneManager.CurrentAsyncLoadPercentage));
                m_loadingBarRect.rect.Set(
                    m_loadingBarRect.rect.x,
                    m_loadingBarRect.rect.y,
                    m_gameSceneManager.CurrentAsyncLoadPercentage * m_fullWidthOfLoadingBar,
                    m_loadingBarRect.rect.height);
            }
        }
    }
}
