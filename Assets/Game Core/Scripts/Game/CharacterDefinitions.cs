using System.Collections.Generic;
using UnityEngine;

namespace CaM.Game
{
    // Contains the different types of Characters within the game that players can pick
    // Allow future implementation to have this information be populated from assets too, hardcoded characters plus modded/additional characters
    public class CharacterDefinitions : MonoBehaviour
    {
        public List<CharacterData> CharacterList;
    }
}
