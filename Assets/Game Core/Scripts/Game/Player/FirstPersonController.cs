using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;

using CaM.Game;

namespace CaM.Game.Player
{
    // there will be a TopDownViewController for Cats/Pred gameplay
    // alongside ThirdPersonController further down the line

    [RequireComponent(typeof(CharacterController)/*, typeof(TopDownViewController), typeof(ThirdPersonController)*/)]
    public class FirstPersonController : NetworkBehaviour
    {
        // Debug target velocity
        [Tooltip("DEBUG: Showing the target velocity of the player")]
        [SerializeField]
        private Vector3 m_targetVelocity;
        [Tooltip("DEBUG: Showing the current character velocity")]
        [SerializeField]
        private Vector3 m_characterVelocity;

        public NetworkVariable<Vector3> pos = new NetworkVariable<Vector3>(NetworkVariableReadPermission.Everyone, Vector3.one);
        void ListenChanges() {
            pos.OnValueChanged += ValueChanged;
        }

        void RemoveChanges() {
            pos.OnValueChanged -= ValueChanged;
        }

        void ValueChanged(Vector3 preV, Vector3 newV) {
            //Debug.Log("pos went from " + preV + " to " + newV);
        }

        // Allows the player to move/FPS camera to be active
        // Does not allow control until the camera is in position, which is SET by an external script. (Likely PlayerMovementManager, that interacts with the Cinemachine scripts)
        [Tooltip("If enabled, would mean player is in First Person Control.")]
        public bool IsInFirstPersonControl = false;

        [Header("Curves")]
        [Tooltip("Curve that controls the movment acceleration")]
        public AnimationCurve MovementCurve;
        [Tooltip("Curve that controls the movment deceleration")]
        public AnimationCurve DecelerationCurve;

        [Header("Movement")]
        [Tooltip("Physics layers to check for a Player Grounded Check")]
        public LayerMask GroundCheckLayers = -1;
        // Checks for if character is grounded
        [Tooltip("Shows if the player is grounded or not")]
        public bool IsGrounded = true;
        private NetworkVariable<bool> m_isGrounded = new NetworkVariable<bool>(NetworkVariableReadPermission.Everyone, true);
        // Controls the sensitivity of camera movement sensitivity
        [Tooltip("Controls the sensitivity of looking around")]
        [Range(0.1f, 1f)]
        public float LookSensitivity = 0.25f;
        // Maximum movment speed when grounded (and not sprinting)
        [Tooltip("Maximum allowed speed when grounded")]
        public float MaxSpeedOnGround = 3f;
        // Max Speed in Air
        [Tooltip("Maximum allowed speed when in the air")]
        public float MaxSpeedInAir = 7f;
        // Multiplcator for sprint speed
        [Tooltip("Speed modifier increase when player is sprinting")]
        public float SprintSpeedModifier = 1.5f;
        // Sharpness for the movement when grounded.
        // A low value will make the player accelerate and decelerate slowly,
        // A high value will do the opposite.
        [Tooltip("Sharpness for the movement when grounded, lower values will slow down player acceleration/deceleration")]
        public float MovementSharpnessOnGround = 5f;
        // Multipler that will propell the player up
        [Tooltip("Force that propells the player up into the air")]
        public float JumpForce = 4f;

        // Player camera, this would be the camera with the Cinemachine Brain component
        private Transform m_playerCameraTransform;
        [SerializeField]
        private Transform m_playerCameraDefaultTransform;

        private Vector3 m_GroundNormal;
        private float m_lastTimeJumped = 0f;
        private float m_groundCheckDistance = 1f;
        private float m_movementTime = 0f;
        private float m_cameraVerticalAngle;

        // Game waits for 0.2 seconds before checking for if player is grounded or not
        const float k_JumpGroundingPreventionTime = 0.2f;
        // Game checks if player is above this height in the air before checking if player is gruonded
        const float k_GroundCheckDistanceInAir = 0.08f;

        // Variables for "Gravity" and controls (movement/look vectors/jump)
        // Below would also include sprinting
        private float m_gravity
        {
            get { return -Physics.gravity.y; }
        }
        private float m_movementX
        {
            get { return m_inputManager.GetPlayerMovement().x; }
        }
        private float m_movementY
        {
            get { return m_inputManager.GetPlayerMovement().y; }
        }
        private float m_lookX
        {
            get { return m_inputManager.GetMouseDelta().x; }
            set { m_lookX = value; }
        }
        private float m_lookY
        {
            get { return m_inputManager.GetMouseDelta().y; }
            set { m_lookY = value; }
        }
        private bool m_jumpButtonPressed
        {
            get { return m_inputManager.PlayerJumpedThisFrame(); }
        }

        

        private PlayerMovementManager m_playerMovementController;
        private InputManager m_inputManager;
        private CharacterController m_characterController;

        private void Awake() {
            // Grab components
            m_playerMovementController = GetComponent<PlayerMovementManager>();
            m_characterController = GetComponent<CharacterController>();
            m_inputManager = GetComponent<InputManager>();
            m_playerCameraTransform = FindObjectOfType<Cinemachine.CinemachineBrain>().transform;

            // Ensures that the centre of the character is in the right position
            // temp solution
            if (m_characterController.center == Vector3.zero)
                m_characterController.center = Vector3.up;
        }

        private void OnEnable() {
            // Lock mouse/curson on screen when the Controller/Component is Enabled
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            ListenChanges();
        }

        private void Start() {
            //Debug.Log("FirstPersonController: START start");

            // Get initial Look values on current Camera & Player position
            //m_lookX = transform.eulerAngles.y;
            //m_lookY = PlayerCameraTransform.eulerAngles.x;

            //Debug.Log("FirstPersonController: START END");

            // WIP/TEMP
            if (IsInFirstPersonControl)
            {
                m_playerCameraTransform.parent = m_playerCameraDefaultTransform;
                m_playerCameraTransform.localPosition = Vector3.zero;
                m_playerCameraTransform.GetComponent<Camera>().orthographic = false;
            }
        }

        private void Update() {
            if (!IsInFirstPersonControl || (IsServer && !IsClient))
                return;

            GroundCheck();

            CharacterMovement();
        }

        private void OnDisable() {
            // Unlock mouse/curson on screen when the Controller/Component is disabled
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            RemoveChanges();
        }






        void GroundCheck() {
            // Make sure that the ground check distance while already in air is very small, to prevent suddenly snapping to ground
            float chosenGroundCheckDistance =
                IsGrounded ? (m_characterController.skinWidth + m_groundCheckDistance) : k_GroundCheckDistanceInAir;

            // reset values before the ground check
            IsGrounded = false;
            m_GroundNormal = Vector3.up;

            // only try to detect ground if it's been a short amount of time since last jump; otherwise we may snap to the ground instantly after we try jumping
            if (Time.time >= m_lastTimeJumped + k_JumpGroundingPreventionTime)
            {
                // if we're grounded, collect info about the ground normal with a downward capsule cast representing our character capsule
                if (Physics.CapsuleCast(GetCapsuleBottomHemisphere(), GetCapsuleTopHemisphere(m_characterController.height),
                    m_characterController.radius, Vector3.down, out RaycastHit hit, chosenGroundCheckDistance, GroundCheckLayers,
                    QueryTriggerInteraction.Ignore))
                {

                    // storing the upward direction for the surface found
                    m_GroundNormal = hit.normal;

                    // Only consider this a valid ground hit if the ground normal goes in the same direction as the character up
                    // and if the slope angle is lower than the character controller's limit
                    if (Vector3.Dot(hit.normal, transform.up) > 0f &&
                        IsNormalUnderSlopeLimit(m_GroundNormal))
                    {
                        IsGrounded = true;

                        // handle snapping to the ground
                        if (hit.distance > m_characterController.skinWidth)
                        {
                            VelocityMoveServerRPC(Vector3.down * hit.distance);
                        }
                    }
                }
            }
            m_isGrounded.Value = IsGrounded;
        }

        void CharacterMovement() {
            // horizontal character rotation
            // rotate the transform with Look vector around local Y-axis
            transform.Rotate(new Vector3(0f, m_lookX * LookSensitivity, 0f), Space.Self);

            // vertical camera rotation
            // add vertical component of Look vector to camera's vertical angle
            m_cameraVerticalAngle += -m_lookY * LookSensitivity;

            // limit camera's verticle angle to -89/+89
            m_cameraVerticalAngle = Mathf.Clamp(m_cameraVerticalAngle, -89f, 89f);

            // apply the vertical angle as a local rotation to the camera transform along
            // its right axis (makes it pivot up and down)
            m_playerCameraTransform.localEulerAngles = new Vector3(m_cameraVerticalAngle, 0f, 0f);



            // apply movement component to calculate movement vector
            Vector3 characterVector = new Vector3(m_movementX, 0.0f, m_movementY);

            // clamp to max magnitude of 1, otherwise diagonal movement might exceed max movement speed defined
            characterVector = Vector3.ClampMagnitude(characterVector, 1);

            // convers move input to a worldspace vector based on character's transform orientation
            Vector3 worldspaceMoveInput = transform.TransformVector(characterVector);

            // Set target velocity
            Vector3 targetVelocity = Vector3.zero;
            //TargetVelocitySyncServerRPC(targetVelocity);

            // ADD SPRINTING HERE
            float speedModifier = /*isSprinting ? SprintSpeedModifier :*/ 1f;



            //speedModifier = Mathf.Clamp(speedModifier, 0f, 1f);

            if (IsGrounded)
            {
                // This if function uses the curves to handle acceleration and deceleration
                if ((m_movementX != 0f || m_movementY != 0f))
                {
                    speedModifier = MovementCurve.Evaluate(m_movementTime);
                    if (m_movementTime <= 1f)
                        m_movementTime += Time.deltaTime;
                }
                else if (m_inputManager.GetPlayerMovement() == Vector2.zero)
                {
                    speedModifier = DecelerationCurve.Evaluate(m_movementTime);
                    if (m_movementTime >= 0f)
                        m_movementTime -= Time.deltaTime;
                }

                // calculated player's velocity from inputs and max speed allowed
                targetVelocity = worldspaceMoveInput * MaxSpeedOnGround * speedModifier;
                m_targetVelocity = targetVelocity;
                TargetVelocitySyncServerRPC(targetVelocity);
                //m_CharacterVelocity = targetVelocity;

                //// Smoothly Interpolate between current velocity and target velocity
                m_characterVelocity = Vector3.Lerp(m_characterVelocity, targetVelocity,
                    MovementSharpnessOnGround * Time.deltaTime);

                if (m_jumpButtonPressed)
                {
                    pos.Value *= 2f;

                    // ensure jump is done properly, vertical component is cancelled out
                    m_characterVelocity = new Vector3(m_characterVelocity.x, JumpForce, m_characterVelocity.z);

                    // add up JumpSpeed
                    //m_CharacterVelocity += Vector3.up * JumpForce;

                    m_lastTimeJumped = Time.time;
                    //m_jumpButtonPressed = false;
                    IsGrounded = false;
                    m_GroundNormal = Vector3.up;
                }
            }
            else
            {
                // add air acceleration
                m_characterVelocity += worldspaceMoveInput * MaxSpeedInAir * Time.deltaTime;

                //// limit air speed to a maximum, but only horizontally
                float verticalVelocity = m_characterVelocity.y;
                Vector3 horizontalVelocity = Vector3.ProjectOnPlane(m_characterVelocity, Vector3.up);
                horizontalVelocity = Vector3.ClampMagnitude(horizontalVelocity, MaxSpeedInAir * speedModifier);
                m_characterVelocity = horizontalVelocity + (Vector3.up * verticalVelocity);

                // apply the gravity to the velocity
                m_characterVelocity += Vector3.down * m_gravity * Time.deltaTime;
            }

            // apply final calculated velocity value to Character Controller
            Vector3 capsuleBottomBeforeMove = GetCapsuleBottomHemisphere();
            Vector3 capsuleTopBeforeMove = GetCapsuleTopHemisphere(m_characterController.height);
            //m_characterController.Move(m_characterVelocity * Time.deltaTime);
            VelocityMoveServerRPC(m_characterVelocity * Time.deltaTime);
            m_isGrounded.Value = IsGrounded;
            // FUTURE USE CLIENT SIDE CALCULATION TO MOVE WHILE WAITING FOR SERVER TO RESPOND (Rollback Netcode)

            // detect obstructions to adjust velocity accordingly
            //m_LatestImpactSpeed = Vector3.zero;
            if (Physics.CapsuleCast(capsuleBottomBeforeMove, capsuleTopBeforeMove, m_characterController.radius,
                m_characterVelocity.normalized, out RaycastHit hit, m_characterVelocity.magnitude * Time.deltaTime, -1,
                QueryTriggerInteraction.Ignore))
            {
                // We remember the last impact speed because the fall damage logic might need it
                //  m_LatestImpactSpeed = CharacterVelocity;

                m_characterVelocity = Vector3.ProjectOnPlane(m_characterVelocity, hit.normal);
            }
        }

        // Gets the center point of the bottom hemisphere of the character controller capsule    
        Vector3 GetCapsuleBottomHemisphere() {
            return transform.position + (transform.up * m_characterController.radius);
        }

        // Gets the center point of the top hemisphere of the character controller capsule    
        Vector3 GetCapsuleTopHemisphere(float atHeight) {
            return transform.position + (transform.up * (atHeight - m_characterController.radius));
        }

        // Returns true if the slope angle represented by the given normal is under the slope angle limit of the character controller
        bool IsNormalUnderSlopeLimit(Vector3 normal) {
            return Vector3.Angle(transform.up, normal) <= m_characterController.slopeLimit;
        }



        // Networking

        [ServerRpc]
        public void VelocityMoveServerRPC(Vector3 velocityMove) {

            MovePlayer(velocityMove);
            VelocityMoveClientRPC(velocityMove);
        }

        [ServerRpc]
        public void TargetVelocitySyncServerRPC(Vector3 targetVelocity) {
            UpdateTargetVelocity(targetVelocity);
            TargetVelocitySyncClientRPC(targetVelocity);
        }

        [ClientRpc]
        public void VelocityMoveClientRPC(Vector3 velocityMove) {
            MovePlayer(velocityMove);
        }

        [ClientRpc]
        public void TargetVelocitySyncClientRPC(Vector3 targetVelocity) {
            UpdateTargetVelocity(targetVelocity);
        }

        void MovePlayer(Vector3 velocity) {
            m_characterController.Move(velocity);
        }

        void UpdateTargetVelocity(Vector3 targetVelocity) {
            m_targetVelocity = targetVelocity;
        }
    }
}
