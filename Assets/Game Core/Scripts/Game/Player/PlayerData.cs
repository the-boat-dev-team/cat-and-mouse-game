using System;
using UnityEngine;

using CaM.Game;

namespace CaM.Game.Player
{
    [Serializable]
    public class PlayerData
    {
        [Tooltip("Character Data for Player")]
        public CharacterData PlayerCharacterData;

        public enum CameraMode { FirstPerson, TopDown, Spectator, MenuSpectator }
        [Tooltip("Camera mode that player is in")]
        public CameraMode PlayerCameraMode = CameraMode.MenuSpectator;

        [Tooltip("Player name")]
        public string PlayerName = "Player #0";

        // Shows what time player joined
        public DateTime JoinTime;
    }
}
