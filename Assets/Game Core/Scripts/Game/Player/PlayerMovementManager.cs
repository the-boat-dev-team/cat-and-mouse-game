using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;

namespace CaM.Game.Player
{
    [RequireComponent(typeof(NetworkObject), typeof(NetworkTransform))]
    public class PlayerMovementManager : NetworkBehaviour
    {
        private NetworkManager m_networkManager;
        //private NetworkObject m_playerNetworkObject;
        private ulong m_playerClientId;

        private FirstPersonController m_firstPersonController;
        private CharacterController m_characterController;

        private void Start() {
            // Assigns the class references
            m_networkManager = NetworkManager.Singleton;
            //m_playerNetworkObject = GetComponent<NetworkObject>();
            m_characterController = GetComponent<CharacterController>();
            m_firstPersonController = GetComponent<FirstPersonController>();

            // obtains player Client Id
            m_playerClientId = GetComponent<NetworkObject>().OwnerClientId;

            // Sets the player's name
            transform.name = "Player #" + m_playerClientId;

            if (!IsLocalPlayer)
                return;

            // (testing) moves player randomly around the map
            //MovePlayerToRandomPos();
        }

        //private void MovePlayerToRandomPos() {
        //    // this should be what is in FirstPersonController, moving locally and calling for ServerRPC

        //    // do random calculations locally
        //    float randX = Random.Range(-2f, 2f);
        //    float randZ = Random.Range(-3f, 3f);
        //    Vector3 randPos = new Vector3(randX, 0f, randZ);

        //    // Sends the command to the server
        //    RandomPosMoveServerRPC(randPos);
        //}

        //[ServerRpc]
        //public void RandomPosMoveServerRPC(Vector3 randPos) {

        //    MovePlayer(randPos);
        //    RandomPosMoveClientRPC(randPos);
        //}

        //[ClientRpc]
        //public void RandomPosMoveClientRPC(Vector3 randPos) {
        //    MovePlayer(randPos);
        //}

        //[ServerRpc]
        //public void VelocityMoveServerRPC(Vector3 velocityMove, Vector3 targetVelocity) {

        //    MovePlayer(velocityMove);
        //    VelocityMoveClientRPC(velocityMove);
        //}

        //[ClientRpc]
        //public void VelocityMoveClientRPC(Vector3 velocityMove, Vector3 targetVelocity) {
        //    MovePlayer(velocityMove);
        //}

        //void MovePlayer(Vector3 velocity, Vector3 targetVelocity) {
        //    //transform.position = pos;
        //    m_characterController.Move(velocity);
        //    m_firstPersonController.
        //}

        //private void OnGUI() {
        //    if (!IsLocalPlayer || !IsServer)
        //        return;
        //    GUILayout.BeginArea(new Rect(10, 10, 300, 300));
        //    if (GUILayout.Button("List OwnerClientIds"))
        //    {
        //        Debug.Log("============================================================================");
        //        Debug.LogWarning("Number of Connected Clients: " + m_networkManager.ConnectedClientsList.Count);
        //        for (int i = 0; i < m_networkManager.ConnectedClientsList.Count; i++)
        //        {
        //            Debug.Log("============================================================================");
        //            Debug.LogWarning("PlayerNetworkManager: ClientId: " + m_networkManager.ConnectedClientsList[i].ClientId + " | PlayerObject name: " + m_networkManager.ConnectedClientsList[i].PlayerObject.name);
        //            Debug.LogWarning("PlayerNetworkManager: OwnedObjects for Client no." + i + ": " + m_networkManager.ConnectedClientsList[i].OwnedObjects.Count);
        //        }
        //        Debug.Log("============================================================================");

        //        List<NetworkObject> noList = FindObjectsOfType<NetworkObject>().ToList();
        //        Debug.LogWarning("Number of NetworkObjects: " + noList.Count);
        //        //for (int i = 0; i < noList.Count; i++)
        //        //{
        //        //    noList[i].OwnerClientId;
        //        //}

        //        Debug.LogWarning("PlayerNetworkManager: Network Manager Local Client Id is... " + m_networkManager.LocalClientId);
        //    }
        //    GUILayout.EndArea();
        //}
    }

}
