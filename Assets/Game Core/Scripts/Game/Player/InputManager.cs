using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CaM.Game.Player
{
    [RequireComponent(typeof(PlayerInput), typeof(FirstPersonController), typeof(PlayerMovementManager))]
    public class InputManager : NetworkBehaviour
    {
        private PlayerControls playerControls;

        private void Awake() {
            playerControls = new PlayerControls();
        }

        private void OnEnable() {
            playerControls.Enable();
        }

        private void OnDisable() {
            playerControls.Disable();
        }

        public Vector2 GetPlayerMovement() {
            return playerControls.Player.Move.ReadValue<Vector2>();
        }

        public Vector2 GetMouseDelta() {
            return playerControls.Player.Look.ReadValue<Vector2>();
        }

        public bool PlayerJumpedThisFrame() {
            return playerControls.Player.Jump.triggered;
        }
    }
}
