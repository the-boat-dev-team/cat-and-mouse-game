using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

using CaM.UI;

namespace CaM.Game.Networking
{
    public class GameNetworkSceneManager : NetworkBehaviour
    {

        private NetworkManager m_networkManager;

        private static GameNetworkSceneManager _instance;
        public static GameNetworkSceneManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public enum GameDefaultSceneNames
        {
            _ManagersScene,
            _GameStartMenu,
            _GameMenuScene,
            _GUIScene,
            _MapLoadingScene,
            _BaseMapScene
        }

        private int m_currentGameMapSceneID = -1;
        // Displays what is the current sceneID for the playing map is
        public int CurrentGameMapSceneID
        {
            get
            {
                return m_currentGameMapSceneID;
            }
        }

        // This stores the list of asyncronous scene loading operations that is current occuring
        private bool m_already100;

        private List<AsyncOperation> m_currentAsyncOperations;
        // This float shows what is the percentage of scene loading that is current done frmo 0 to 1
        public float CurrentAsyncLoadPercentage
        {
            get
            {
                if (m_currentAsyncOperations != null)
                {
                    if (m_currentAsyncOperations.Count == 0)
                    {
                        return 1f;
                    }

                    // Using LinQ instead of for-loop
                    // Source: https://www.toptal.com/c-sharp/top-10-mistakes-that-c-sharp-programmers-make
                    float total = (from asyncOperation in m_currentAsyncOperations
                                   select Mathf.Clamp01(asyncOperation.progress / 0.9f)).Sum();

                    // Takes the total loading currently and averages it
                    float average = total / m_currentAsyncOperations.Count;
                    if (!m_already100)
                    {
                        Debug.Log("GameNetworkSceneManager: Current Async Loading percentage " + (average * 100) + "%, count: " + m_currentAsyncOperations.Count, this);
                        if (average == 1)
                            m_already100 = true;
                    }
                    
                    if (float.IsNaN(average))
                    {
                        Debug.LogError("GameNetworkSceneManager: Accidentally divided 0 by 0. Counting average of async operations didn't work out.", this);
                        return 0f;
                    }
                    return average;
                }
                Debug.LogWarning("GameNetworkSceneManager: -1", this);
                return -1f;
            }
        }

        // Scenes
        //// 0 - Managers Scene
        // Contains NetworkManager, PlayerInputManager, GameNetworkSceneManager, UserSettingsManager <soon>, GameplayManager <soon>, GameNetworkManager <soon>
        //// 1 - Game Start Menu
        // Contains UI for Networking (Host a game, start a server, join a game), settings, and edit
        // Can be loaded in via [Scene 0] or [Scene 3]
        //// 2 - Game Menu Scene <soon>
        // Contains the Pause Menu, and settings, contains UI for character selection
        //// 3 - GUI Scene <soon>
        // Contains the Game UI, everything required to show the user information and data (chat/health/timer/gameplay specific UI)
        //// 4 - Map Loading Scene <soon>
        // Overlays on top of everything else as the game is loading in asyncronously
        //// 5 - Base Map Scene
        // Contains the base map logic (PnP.Gameplay logic)
        // GameplayManager keeps track of how many gameplay scenes there are (to show to UI and server selection)
        //// 6+ - Different gameplays and maps will have different scenes
        // Any logic assosiated with the map (PnP.Gameplay.XXX logic)

        // Scene Bundles
        // > PROGRAM START <
        // 0
        // > START MENU <
        // (0) + 1
        // > GAME LOAD <
        // (0) + 4 => 5 + 6+ => 2 + 3
        // > GAME START <
        // (0) + 5 + 6+ + 2 + 3
        // > RETURN TO START MENU <
        // (0) + 1

        private void Awake() {
            // Check if this is the only instance and if not, yeet it away
            if (_instance != null && _instance != this) { Destroy(this); }
            else { _instance = this;  }

            // Get the async list initialised
            m_currentAsyncOperations = new List<AsyncOperation>();

            // When the game starts, load the "Game Start Menu" scene immediately to start the game
            SceneManager.LoadScene(1, LoadSceneMode.Additive);
        }

        private void OnEnable() {
            SceneManager.sceneLoaded += startNetworking;
        }

        private void Start() {
            // Get NetworkManager
            m_networkManager = NetworkManager.Singleton;

            // Unload the Manager scene
            StartCoroutine(StartUnload(0, UnloadSceneOptions.None));
        }

        private void OnDisable() {
            SceneManager.sceneLoaded -= startNetworking;
        }

        // this is run by the local player when scenes are loaded in
        // this checks for when the desired game map has been loaded, then starts the server
        // also then randomly generates a position between (x:-2 / z:-3) and (x:2 / z:3)
        private void startNetworking(Scene arg0, LoadSceneMode arg1) {

            if (arg0.buildIndex <= 5)
                return;
            else if (arg0.buildIndex == CurrentGameMapSceneID)
            {
                // this always ensures the GameMapScene is always set as the active one
                SceneManager.SetActiveScene(arg0);

                // this will wait until the scenes are finished loading before networking is started and the loading screen will be unloaded
                StartCoroutine(LoadNetworking(arg0));
            }
        }

        //private void OnGUI() {

        //    if (!temp_showGUI)
        //        return;

        //    GUILayout.BeginArea(new Rect(300, 10, 200, 200));
        //    if (GUILayout.Button("Test spawnedObjectList count"))
        //    {
        //        HashSet<NetworkObject> spawnedObjectsList = m_networkManager.SpawnManager.SpawnedObjectsList;
        //        Debug.LogError("spawnedObjectsList list count is: " + spawnedObjectsList.Count);
        //    }
        //    GUILayout.EndArea();
        //}

        public void LoadGameFromStartMenu(int gameMapScene) {

            // Load "Map Loading Scene"
            // This is just a loading screen
            SceneManager.LoadScene(4, LoadSceneMode.Single);

            // Load "Base Map Scene"
            StartCoroutine(StartLoad(GameDefaultSceneNames._BaseMapScene, LoadSceneMode.Additive));
            //SceneManager.LoadSceneAsync(5, LoadSceneMode.Additive);

            // if gameMapScene is below 6, it'll default to the default map
            if (gameMapScene < 6) gameMapScene = 6;
            m_currentGameMapSceneID = gameMapScene;

            // Load the scene of the desired Gameplay Map
            StartCoroutine(StartLoad(gameMapScene, LoadSceneMode.Additive));
            //SceneManager.LoadSceneAsync(gameMapScene, LoadSceneMode.Additive);

            // Load "Game Menu Scene"
            StartCoroutine(StartLoad(GameDefaultSceneNames._GameMenuScene, LoadSceneMode.Additive));
            //SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive);

            // Load "GUI Scene"
            StartCoroutine(StartLoad(GameDefaultSceneNames._GUIScene, LoadSceneMode.Additive));
            //SceneManager.LoadSceneAsync(3, LoadSceneMode.Additive);
        }

        public void UnloadGame() {

        }

        public void ReturnToMainMenu() {

        }

        // loads the scene and keeps track of the percentage for the loading screen bar
        IEnumerator StartLoad(GameDefaultSceneNames sceneName, LoadSceneMode loadMode) {


            // Give it more delay
            //yield return new WaitForSecondsRealtime(Random.Range(0.5f, 3f));

            // Loads it like any other async scene, but keeps track of it within this function
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName.ToString(), loadMode);
            // adds the operation into the list to keep track of loading percentage
            m_currentAsyncOperations.Add(operation);

            // waits for the operation to complete
            while (!operation.isDone)
            {
                yield return null;
            }

            // operation is complete, removes it from the list
            m_currentAsyncOperations.Remove(operation);
            yield return null;
        }

        // same as above, but takes the sceneID instead
        IEnumerator StartLoad(int sceneID, LoadSceneMode loadMode) {

            // Give it more delay
            //yield return new WaitForSecondsRealtime(Random.Range(0.5f, 3f));

            // Loads it like any other async scene, but keeps track of it within this function
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneID, loadMode);
            // adds the operation into the list to keep track of loading percentage
            m_currentAsyncOperations.Add(operation);

            // waits for the operation to complete
            while (!operation.isDone)
            {
                yield return null;
            }

            // operation is complete, removes it from the list
            m_currentAsyncOperations.Remove(operation);
            yield return null;
        }

        // unload the scene
        IEnumerator StartUnload(int sceneID, UnloadSceneOptions unloadMode) {

            // Loads it like any other async scene, but keeps track of it within this function
            AsyncOperation operation = SceneManager.UnloadSceneAsync(sceneID, unloadMode);

            // waits for the operation to complete
            while (!operation.isDone)
            {
                yield return null;
            }

            yield return null;
        }

        IEnumerator LoadNetworking(Scene scene) {
            while (CurrentAsyncLoadPercentage < 1f)
            {
                yield return null;
            }

            // wait 1 second before starting networking
            yield return new WaitForSecondsRealtime(1f);

            if (LoadingData.isServer && LoadingData.isClient)
                m_networkManager.StartHost();
            else if (LoadingData.isClient)
                m_networkManager.StartClient();
            else if (LoadingData.isServer)
                m_networkManager.StartServer();
            else
                Debug.LogError("GameNetworkSceneManager: Player is neither server nor client, network was not initiated.");

            // wait 1 second before unloading the loading screen
            // FUTURE: Check for when player has connected before removing the loading screen
            yield return new WaitForSecondsRealtime(1f);

            FindObjectOfType<MapLoadingMenuManager>().GetComponent<Animator>().SetBool("SceneLoaded", true);

            yield return null;
        }
    }
}
