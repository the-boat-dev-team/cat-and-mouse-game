using Cinemachine;
using UnityEngine;

using CaM.Game.Player;

namespace CaM.Game
{

    // WIP
    // This script extends Cinemachine to be controlled by a player in a First-person/Point-of-view perspective
    public class CinemachinePOVExtension : CinemachineExtension
    {
        private FirstPersonController m_fpController;
        private Vector3 m_StartingRotation;

        protected override void Awake() {
            base.Awake();
        }

        protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime) {
            if (vcam.Follow)
            {
                if (stage == CinemachineCore.Stage.Aim)
                {
                    if (m_StartingRotation == null) m_StartingRotation = transform.localRotation.eulerAngles;
                    //Vector2 deltaInput = 
                    throw new System.NotImplementedException();
                }
            }
        }
    }
}
