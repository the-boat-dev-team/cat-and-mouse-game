using System;
using UnityEngine;

namespace CaM.Game
{
    // Class that contains the data for gameplay attached to the Player
    // Would a struct or static be appropraite for this?
    // What implementation would be better for ease of character addition in the future?
    // Or if mod implementation would be affected or if it should even be a consideration at all?


    // abstract??? interface???
    public class CharacterData
    {
        public enum CharacterType { NONE, Cat, Mouse }
        //[Tooltip("Type of character player is")]
        public CharacterType PlayerCharacterType { get; set; }

        //[Tooltip("TEMP: Material/colour that character is shown as")]
        public Material CharacterMaterial { get; set; }
    }
}
